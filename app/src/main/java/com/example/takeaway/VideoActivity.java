

package com.example.takeaway;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;
import com.example.takeaway.Adapter.GalleryAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.imbryk.viewPager.LoopViewPager;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import me.relex.circleindicator.Config;

public class VideoActivity extends AppCompatActivity {
    private int choice;
    DatabaseReference mReference;
    List<String> nameofList;
    List<String> nameofImages;
    LoopViewPager viewpager;
    VideoView videoView;
    ImageView logo;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
       setupData();
       initView();
       intiFields();
    }

    private void setupData() {
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // String value = dataSnapshot.getValue(String.class);
                choice  = (int) dataSnapshot.getChildrenCount();
                nameofList = new ArrayList<String>();
                nameofImages = new ArrayList<String>();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String answerName = ds.getKey();
                    nameofList.add(answerName);
                    String imgeUrl = ds.child("imageUrl").getValue(String.class);
                    if(imgeUrl != null){
                        nameofImages.add(imgeUrl);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    private void intiFields() {
        viewpager.setOnViewPagerClickListener(new LoopViewPager.OnClickListener() {
            @Override
            public void onViewPagerClick(ViewPager viewPager) {
                Intent intent = new Intent(VideoActivity.this, QuizchallengeActivity.class);
                intent.putExtra("bestitz",getChoice() + "");
                intent.putExtra("NAMEOFANSWERS",(ArrayList<String>)getNameofList());
                intent.putExtra("NAMEOFIMAGES",(ArrayList<String>)getNameofImages());
                startActivity(intent);
            }
        });
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.feeling_hungry);

        MediaController controller = new MediaController(this);

        controller.setAnchorView(videoView);

        videoView.setMediaController(controller);
        logo.setImageResource(R.drawable.logo2);
        textView.setText("Gallery/Enter CodeChallenge");
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(20);

    }

    private void initView() {
        videoView = findViewById(R.id.videoView);
        viewpager = findViewById(R.id.viewpager);
        logo = findViewById(R.id.logo);
        textView = findViewById(R.id.textView);
        CircleIndicator indicator = findViewById(R.id.indicator);

        int indicatorWidth = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,
                getResources().getDisplayMetrics()) + 0.5f);
        int indicatorHeight = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4,
                getResources().getDisplayMetrics()) + 0.5f);
        int indicatorMargin = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6,
                getResources().getDisplayMetrics()) + 0.5f);
        Config config = new Config.Builder().width(indicatorWidth)
                .height(indicatorHeight)
                .margin(indicatorMargin)
                .animator(R.animator.indicator_animator)
                .animatorReverse(R.animator.indicator_animator_reverse)
                .drawable(R.drawable.black_radius_square)
                .build();
        indicator.initialize(config);
        viewpager.setAdapter(new GalleryAdapter());
        indicator.setViewPager(viewpager);
    }

    public int getChoice() {
        return choice;
    }

    @Override
    public void onBackPressed() {
        Intent formIntent = new Intent(VideoActivity.this, WelcomeActivity.class);
        startActivity(formIntent);
    }

    public List<String> getNameofList() {
        return nameofList;
    }

    public List<String> getNameofImages() {
        return nameofImages;
    }
}
