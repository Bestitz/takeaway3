
/*
 * *
 *   Copyright (C) 2019
 *   Author : Ruikang Xu
 *
 *
 */

package com.example.takeaway;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.takeaway.EditAdapter.EditAnswerAdapter;
import com.example.takeaway.EditAdapter.EditCodeAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class EditActivity extends AppCompatActivity {
    private EditCodeAdapter mAdapter;
    private EditAnswerAdapter answerAdapter;
    private Button addButton;
    private Button formBtn;
    private DatabaseReference mReference;
    private static int numofQuizs;
    ViewPager viewPager3, viewpager;
    ImageView logo;
    private ArrayList<String> nameofAnswers,nameofimages;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        if(getIntent().getStringExtra("bestitz") != null){
            setNumofQuizs(Integer.parseInt(getIntent().getStringExtra("bestitz")));
        }
        if(getIntent().getStringArrayListExtra("NAMEOFIMAGES") != null){
            this.nameofimages = getIntent().getStringArrayListExtra("NAMEOFIMAGES");
        }
        if(getIntent().getStringArrayListExtra("NAMEOFANSWERS") != null){
            this.nameofAnswers = getIntent().getStringArrayListExtra("NAMEOFANSWERS");
        }
        //test
        initFieldAndViews();
        upload();
        addNewQuiz();
        deleteQuiz();
        mReference = FirebaseDatabase.getInstance().getReference();
    }

    private ArrayList<String> getNameofImages(){
        return nameofimages;
    }

    private void addNewQuiz() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nextActivity = new Intent(EditActivity.this, NewQuizActivity.class);
                startActivity(nextActivity);
            }
        });
    }

    public void initFieldAndViews() {
        mAdapter = new EditCodeAdapter(numofQuizs,getNameofimages());
        answerAdapter = new EditAnswerAdapter(numofQuizs,getNameofAnswers());
        addButton = (Button) findViewById(R.id.add);
        addButton.setText("Add");
        logo = findViewById(R.id.logo);
        formBtn = findViewById(R.id.formBtn);
        formBtn.setText("Delete");
        viewpager = findViewById(R.id.choosedImage);
        viewPager3 = findViewById(R.id.newAnswers);
        CircleIndicator indicator = findViewById(R.id.indicator2);
        viewPager3.setAdapter(answerAdapter);
        viewpager.setAdapter(mAdapter);
        indicator.setViewPager(viewPager3);
        logo.setImageResource(R.drawable.logo);
        mAdapter.registerDataSetObserver(indicator.getDataSetObserver());
        answerAdapter.registerDataSetObserver(indicator.getDataSetObserver());
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int mScrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
                viewPager3.scrollTo(viewpager.getScrollX(), viewPager3.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {
                mScrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewPager3.setCurrentItem(viewpager.getCurrentItem(), false);
                }
            }
        });

        viewPager3.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int mScrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
                viewpager.scrollTo(viewPager3.getScrollX(), viewpager.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {
                mScrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewpager.setCurrentItem(viewPager3.getCurrentItem(), false);
                }
            }
        });

    }

    private void deleteQuiz() {
       formBtn.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(EditActivity.this, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    onBackPressed();
                    return super.onDoubleTap(e);
                }
                @Override
                public boolean onSingleTapConfirmed(MotionEvent event) {
                    dialog();
                    Toast.makeText(EditActivity.this, R.string.singleTapOnEdit, Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                gestureDetector.onTouchEvent(event);
                return true;
            }


        });
    }

    private void dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditActivity.this);
        builder.setMessage(R.string.deleteHint);
        builder.setTitle(R.string.deleteTitle);
        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int position = viewPager3.getCurrentItem();
                int position1 = viewpager.getCurrentItem();
                mReference = FirebaseDatabase.getInstance().getReference();
                DatabaseReference answerCollection = mReference.child("answerCollection").child(nameofAnswers.get(position));
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference gsReference = storage.getReferenceFromUrl(nameofimages.get(position1));
                gsReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // File deleted successfully
                        mAdapter.notifyDataSetChanged();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Uh-oh, an error occurred!
                    }
                });

                answerCollection.removeValue();
                answerAdapter.notifyDataSetChanged();
                dialog.dismiss();


                refresh(new MyCallback() {
                    @Override
                    public void onCallback(int value,ArrayList<String> a1,ArrayList<String> a2) {
                        Intent intent;
                        intent = new Intent(EditActivity.this, EditActivity.class);
                        intent.putExtra("bestitz",value + "");
                        intent.putExtra("NAMEOFANSWERS",(ArrayList<String>) a1);
                        intent.putExtra("NAMEOFIMAGES",(ArrayList<String>)a2);
                        startActivity(intent);
                    }
                });
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private void refresh(final MyCallback myCallback){
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference nwAnswerCollection = mReference.child("answerCollection");
        nwAnswerCollection.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
               ArrayList<String> nwnameofAnswers = new ArrayList<String>();
                ArrayList<String> nwnameofimages = new ArrayList<String>();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String answerName = ds.getKey();
                    nwnameofAnswers.add(answerName);
                    String imgeUrl = ds.child("imageUrl").getValue(String.class);
                    nwnameofimages.add(imgeUrl);
                }
                myCallback.onCallback((int) dataSnapshot.getChildrenCount(),nwnameofAnswers,nwnameofimages);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    public interface MyCallback {
        void onCallback(int value,ArrayList<String> arr1,ArrayList<String> arr2);
    }

    public void setNumofQuizs(int num) {
        this.numofQuizs = num;
    }

    private void upload(){
        viewpager.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(EditActivity.this, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    Intent intent = new Intent(EditActivity.this, UpdateImageActivity.class);
                    intent.putExtra("NAMEOFIMAGES",(ArrayList<String>)getNameofImages());
                    intent.putExtra("position",nameofimages.get(viewpager.getCurrentItem()) + "");
                    startActivity(intent);
                    return super.onDoubleTap(e);
                }
                @Override
                public boolean onSingleTapConfirmed(MotionEvent event) {

                    Toast.makeText(EditActivity.this, "Swipe non-Image area to choose questions, doule Tap to Update image", Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                gestureDetector.onTouchEvent(event);
                return true;
            }


        });
    }

    public ArrayList<String> getNameofAnswers() {
        return nameofAnswers;
    }

    public ArrayList<String> getNameofimages() {
        return nameofimages;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EditActivity.this, AdminActivity.class);
        startActivity(intent);
    }


}

