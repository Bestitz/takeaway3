package com.example.takeaway.Moduel;

import java.util.Date;

public class Applicant {
    private String fullName;
    private String email;
    private String dateOfBirth;
    private String contactsNum;
    private static int Num;
    private int ID;
    private String submissionDate;
    private String quizeAnswers;
    public Applicant(){}

    public Applicant(String fullName, String email, String dateOfBirth, String submissionDate,String quizeAnswers,String contactsNum) {
        this.fullName = fullName;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.ID = ++Num;
        this.submissionDate = submissionDate;
        this.quizeAnswers = quizeAnswers;
        this.contactsNum = contactsNum;
    }

    public String getContactsNum() {
        return contactsNum;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public static int getNum() {
        return Num;
    }

    public static void setNum(int num) {
        Num = num;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(String submissionDate) {
        this.submissionDate = submissionDate;
    }

    public String getQuizeAnswers() {
        return quizeAnswers;
    }

    public void setQuizeAnswers(String quizeAnswers) {
        this.quizeAnswers = quizeAnswers;
    }

    public void setContactsNum(String contactsNum) {
        this.contactsNum = contactsNum;
    }
}
