package com.example.takeaway;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.example.takeaway.Moduel.Applicant;
import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ShowApplicantsActivity extends AppCompatActivity {
    DatabaseReference reference;
    ArrayList<Applicant> applicantsList;
    FirebaseListAdapter<Applicant> adapter,searchAdpater;
    ListView listView;
    ImageView logo,backicon;
    EditText search;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.applicant_info);
        initViewsAndFields();
        searchByAnsers();
        deletedOverDue();
    }

    private void searchByAnsers() {
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    search.setSelection(s.toString().length());
                    Query query = reference.orderByChild("quizeAnswers").equalTo(s.toString().toUpperCase());
                    FirebaseListOptions<Applicant> options = new FirebaseListOptions.Builder<Applicant>()
                            .setQuery(query, Applicant.class)
                            .setLayout(R.layout.custome_list)
                            .build();
                    searchAdpater = new FirebaseListAdapter<Applicant>(options) {

                        @Override
                        protected void populateView(@NonNull View v, @NonNull Applicant model, int position) {
                                TextView fullName = v.findViewById(R.id.fullNameTV);
                                TextView email = v.findViewById(R.id.emailTV);
                                TextView contactNum = v.findViewById(R.id.contactNumTV);
                                TextView qzAnswers = v.findViewById(R.id.qzAnswersTV);

                                fullName.setText(model.getFullName());
                                email.setText(model.getEmail());
                                contactNum.setText(model.getContactsNum());
                                email.setText(model.getEmail());
                                qzAnswers.setText(model.getQuizeAnswers());
                        }
                    };
                    listView.setAdapter(searchAdpater);
                    searchAdpater.startListening();
                } else {
                    Query query = reference;
                    FirebaseListOptions<Applicant> options = new FirebaseListOptions.Builder<Applicant>()
                            .setQuery(query, Applicant.class)
                            .setLayout(R.layout.custome_list)
                            .build();
                    adapter = new FirebaseListAdapter<Applicant>(options) {

                        @Override
                        protected void populateView(View v, Applicant model, int position) {
                            TextView fullName = v.findViewById(R.id.fullNameTV);
                            TextView email = v.findViewById(R.id.emailTV);
                            TextView contactNum = v.findViewById(R.id.contactNumTV);
                            TextView qzAnswers = v.findViewById(R.id.qzAnswersTV);

                            fullName.setText(model.getFullName());
                            email.setText(model.getEmail());
                            contactNum.setText(model.getContactsNum());
                            email.setText(model.getEmail());
                            qzAnswers.setText(model.getQuizeAnswers());

                        }
                    };
                    listView.setAdapter(adapter);
                    adapter.startListening();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
        if(searchAdpater !=null){
            searchAdpater.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
        if(searchAdpater !=null){
            searchAdpater.stopListening();
        }
    }



    private void initViewsAndFields() {
        search = findViewById(R.id.search);
        logo = findViewById(R.id.logo);
        logo.setImageResource(R.drawable.logo);
        backicon = findViewById(R.id.formBtn);
        backicon.setImageResource(R.drawable.backicon);
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        reference = FirebaseDatabase.getInstance().getReference().child("Applicants");
        listView = findViewById(R.id.listView);
        Query query = reference;
        FirebaseListOptions<Applicant> options = new FirebaseListOptions.Builder<Applicant>()
                .setQuery(query, Applicant.class)
                .setLayout(R.layout.custome_list)
                .build();
        adapter = new FirebaseListAdapter<Applicant>(options) {

            @Override
            protected void populateView(View v, Applicant model, int position) {
                TextView fullName = v.findViewById(R.id.fullNameTV);
                TextView email = v.findViewById(R.id.emailTV);
                TextView contactNum = v.findViewById(R.id.contactNumTV);
                TextView qzAnswers = v.findViewById(R.id.qzAnswersTV);

                fullName.setText(model.getFullName());
                email.setText(model.getEmail());
                contactNum.setText(model.getContactsNum());
                email.setText(model.getEmail());
                qzAnswers.setText(model.getQuizeAnswers());

            }
        };
        listView.setAdapter(adapter);
    }


    @Override
    public void onBackPressed() {
       Intent intent = new Intent(ShowApplicantsActivity.this,AdminActivity.class);
       startActivity(intent);
    }

    private void deletedOverDue(){
        reference = FirebaseDatabase.getInstance().getReference().child("Applicants");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String pattern = "yyyy-MM-dd";
                Date currentTIME = new Date();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    try {
                        Date date = simpleDateFormat.parse(ds.child("submissionDate").getValue(String.class));
                        Calendar cal1 = Calendar.getInstance();
                        Calendar cal2 = Calendar.getInstance();
                        cal1.setTime(currentTIME);
                        cal2.setTime(date);
                        int yearDiff = cal1.get(Calendar.YEAR) - cal2.get(Calendar.YEAR);
                        int monthDiff = cal1.get(Calendar.MONTH) - cal2.get(Calendar.MONTH);
                        int dayDiff = cal1.get(Calendar.DAY_OF_MONTH) - cal2.get(Calendar.DAY_OF_MONTH);
                        if(yearDiff != 0){
                            DatabaseReference nodeToRemove = ds.getRef();
                            nodeToRemove.removeValue();
                        } else {
                            if(monthDiff > 1 ){
                                DatabaseReference nodeToRemove = ds.getRef();
                                nodeToRemove.removeValue();
                            }else if(monthDiff == 1 && (cal1.get(Calendar.DAY_OF_MONTH) + (31 - cal2.get(Calendar.DAY_OF_MONTH)) >= 30)) {
                                DatabaseReference nodeToRemove = ds.getRef();
                                nodeToRemove.removeValue();
                            }else {
                                if(dayDiff >= 30 ){
                                    DatabaseReference nodeToRemove = ds.getRef();
                                    nodeToRemove.removeValue();
                                }
                            }
                        }

                    } catch (ParseException e) {
                    e.printStackTrace();
                }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    class CustoumAdabter extends BaseAdapter {

        @Override
        public int getCount() {
            return applicantsList.size();
        }

        @Override
        public Object getItem(int position) {
            return applicantsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return applicantsList.indexOf(position);
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.custome_list,viewGroup,false);

            TextView fullName = findViewById(R.id.fullNameTV);
            TextView email = findViewById(R.id.emailTV);
            TextView contactNum = findViewById(R.id.contactNumTV);
            TextView qzAnswers = findViewById(R.id.qzAnswersTV);

            fullName.setText(applicantsList.get(i).getFullName());
            email.setText(applicantsList.get(i).getEmail());
            contactNum.setText(applicantsList.get(i).getContactsNum());
            email.setText(applicantsList.get(i).getEmail());
            qzAnswers.setText(applicantsList.get(i).getQuizeAnswers());

            return view;
        }
    }

}
