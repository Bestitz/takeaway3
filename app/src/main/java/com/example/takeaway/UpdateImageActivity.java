
/*
 * *
 *   Copyright (C) 2019
 *   Author : Ruikang Xu
 *
 *
 */

package com.example.takeaway;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class UpdateImageActivity  extends AppCompatActivity {
    private Button btnChoose, btnUpload;
    private PhotoView imageView;
    private String urlPosition;
    private Uri filePath;
    private int choice;
    private boolean isEditMode;
    private ArrayList<String> nameofImages;
    private ArrayList<String> nameofList;
    DatabaseReference mReference;
    FirebaseStorage storage;
    StorageReference storageReference;
    private final int PICK_IMAGE_REQUEST = 71;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updateimages);
        //get data from intent
        if(getIntent().getStringArrayListExtra("NAMEOFIMAGES") != null){
            nameofImages = (getIntent().getStringArrayListExtra("NAMEOFIMAGES"));
        }
        if(getIntent().getStringExtra("addImage") != null){
            isEditMode = false;
        } else {
            isEditMode = true;
        }
        if(getIntent().getStringExtra("position") != null){
            this.urlPosition = getIntent().getStringExtra("position");
        }
        setupData();
        initViews();
        initFields();
    }

    private void initFields() {
        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });
        if (isEditMode) {
            btnUpload.setText("Update");
            btnUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uploadImage();
                }
            });
        } else {
            btnUpload.setText("Confirm");
            btnUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent;
                    if(imageView.getDrawable() != null){
                        Bitmap bitmap = setimage(imageView);
                        //调用setimage方法，得到返回值bitmap
                        intent = new Intent(UpdateImageActivity.this,NewQuizActivity.class);
                        if(filePath != null){
                            intent.putExtra("FILEPATH",getFilePath().toString());
                        }
                        startActivity(intent);
                    } else {
                        Toast.makeText(UpdateImageActivity.this, "Image is not selected", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void initViews() {
        //Initialize Views
        btnChoose = (Button) findViewById(R.id.btnChoose);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        imageView = (PhotoView) findViewById(R.id.imgView);
    }

    private void setupData() {
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        //refresh data
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // String value = dataSnapshot.getValue(String.class);
                choice  = (int) dataSnapshot.getChildrenCount();
                if(getIntent().getStringArrayListExtra("NAMEOFIMAGES") == null){
                    nameofImages = new ArrayList<String>();
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        String imgeUrl = ds.child("imageUrl").getValue(String.class);
                        nameofImages.add(imgeUrl);
                    }
                }
                nameofList = new ArrayList<String>();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String answerName = ds.getKey();
                    nameofList.add(answerName);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            setFilePath(data.getData());
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), getFilePath());
                imageView.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent;
        Log.d("CDA", "onBackPressed Called");
        if(isEditMode){
            intent = new Intent(UpdateImageActivity.this, EditActivity.class);
            intent.putExtra("bestitz",getChoice() + "");
            intent.putExtra("NAMEOFANSWERS",(ArrayList<String>)getNameofList());
            intent.putExtra("NAMEOFIMAGES",(ArrayList<String>)getNameofImages());

        } else {
            intent = new Intent(UpdateImageActivity.this, NewQuizActivity.class);
        }
        startActivity(intent);
    }

    public ArrayList<String> getNameofImages() {
        return nameofImages;
    }

    public ArrayList<String> getNameofList() {
        return nameofList;
    }

    private void uploadImage() {
        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            StorageReference ref = storage.getReferenceFromUrl(urlPosition);
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            new CountDownTimer(2500, 1000) {
                                public void onFinish() {
                                    // When timer is finished
                                    // Execute your code here
                                    onBackPressed();
                                }

                                public void onTick(long millisUntilFinished) {
                                    // millisUntilFinished    The amount of time until finished.
                                }
                            }.start();
                            Toast.makeText(UpdateImageActivity.this, R.string.uploaded, Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(UpdateImageActivity.this, R.string.failed+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage(R.string.uploaded+(int)progress+"%");
                        }
                    });
        }
    }

    public int getChoice() {
        return choice;
    }

    //把图片转换成bitmap形式传递通过intent形式传递过去
    private Bitmap setimage(ImageView view1){
        Bitmap image = ((BitmapDrawable)view1.getDrawable()).getBitmap();
        Bitmap bitmap1 = Bitmap.createBitmap(image);
        return bitmap1;
    }

    //bitmap转换成byte形式
    private byte[] Bitmap2Bytes(Bitmap bm){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }

    public Uri getFilePath() {
        return filePath;
    }
}
