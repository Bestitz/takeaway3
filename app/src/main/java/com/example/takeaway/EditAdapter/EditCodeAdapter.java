
/*
 * *
 *   Copyright (C) 2019
 *   Author : Ruikang Xu
 *
 *
 */


package com.example.takeaway.EditAdapter;


import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Random;

public class EditCodeAdapter extends PagerAdapter {
    private int numOfQuiz;
    private String imageSource;
    private DatabaseReference mReference;
    private ArrayList<String> namesofImages;
    FirebaseStorage storage;
    StorageReference storageRef;

    public EditCodeAdapter() {
        numOfQuiz = 1;
    }

    public EditCodeAdapter(int count,ArrayList<String> arrayList) {
       this.numOfQuiz = count;
        setNamesofImages(arrayList);
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
        mReference = FirebaseDatabase.getInstance().getReference();
    }

    public void setNamesofImages(ArrayList<String> arr){
        this.namesofImages = arr;
        notifyDataSetChanged();
    }

    @Override public int getCount() {
        return numOfQuiz;
    }

    @Override public int getItemPosition(@NonNull Object object) {
        if (namesofImages.contains(object)) {
            return namesofImages.indexOf(object);
        } else {
            return POSITION_NONE;
        }
    }

    @Override public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup view, int position, @NonNull Object object) {
        view.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, final int position) {
        final ImageView imageView = new ImageView(view.getContext());
        StorageReference gsReference = storage.getReferenceFromUrl(namesofImages.get(position));
        gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                String path = uri.toString();
                /// The string(file link) that you need
                imageSource = path;
                Picasso.get().load(imageSource).into(imageView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
        view.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        return imageView;
    }

}