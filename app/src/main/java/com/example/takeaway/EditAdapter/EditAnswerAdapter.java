
/*
 * *
 *   Copyright (C) 2019
 *   Author : Ruikang Xu
 *
 *
 */

package com.example.takeaway.EditAdapter;

import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class EditAnswerAdapter extends PagerAdapter {

    private int numOfQuiz;
    private DatabaseReference mReference;
    private final static int NUMOFANSWER = 6;
    private static int[] choice;
    public  boolean isAllHit;
    ArrayList<String> namesofQuiz;
    public EditAnswerAdapter() {
        numOfQuiz = 1;
    }

    public EditAnswerAdapter(int count,ArrayList<String> arrayList) {
        numOfQuiz = count;
        this.namesofQuiz = arrayList;
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                // String value = dataSnapshot.getValue(String.class);
                choice  = new int[(int) dataSnapshot.getChildrenCount()];
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    @Override
    public int getCount() {
        return numOfQuiz;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup view, int position, @NonNull Object object) {
        view.removeView((View) object);
    }

    public ArrayList<String> getNamesofQuiz() {
        return this.namesofQuiz;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull  ViewGroup view, final int position) {
        mReference = FirebaseDatabase.getInstance().getReference();
        //通过键名，获取数据库实例对象的子节点对象
        ScrollView scrollView = new ScrollView(view.getContext());
       // RadioGroup radioGroup = new RadioGroup(view.getContext());
        LinearLayout linearLayout = new LinearLayout(view.getContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        //ArrayList<String> lklk = getNamesofQuiz();
        for (int i = 0; i < NUMOFANSWER; i++) {
            LinearLayout choice = new LinearLayout(view.getContext());
            TextView alphaLetter = new TextView(view.getContext());
            String value = "";
            switch (i + 1) {
                case 1:
                    value = "A: ";
                    break;
                case 2:
                    value = "B: ";
                    break;
                case 3:
                    value = "C: ";
                    break;
                case 4:
                    value = "D: ";
                    break;
                case 5:
                    value = "E: ";
                    break;
                case 6:
                    value = "F: ";
                    break;
            }
            alphaLetter.setText(value);
            alphaLetter.setGravity(Gravity.CENTER);
            alphaLetter.setTextColor(Color.WHITE);
            alphaLetter.setTextSize(48);
            alphaLetter.setId(i);
            DatabaseReference answer;
            answer = mReference.child("answerCollection").child(getNamesofQuiz().get(position)).child("text" + String.valueOf(i));
            EditText textView = new EditText(view.getContext());
            readData(new callbackforReading() {
                @Override
                public void onCallback(String value,EditText radioButton) {
                    //radioButton.setText(value);
                    if (value != null){
                        radioButton.setText(value);
                    }

                }
            },answer,textView);
            Log.d("jiku", "instantiateItem: " + textView.getText().toString() );
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(Color.WHITE);
            textView.setTextSize(48);
            textView.setId(i);
            textView.addTextChangedListener(new MyClickListener(i,position,textView));
            choice.addView(alphaLetter);
            choice.addView(textView);
            linearLayout.addView(choice);
        }
        scrollView.addView(linearLayout);
        scrollView.setBackgroundColor(Color.TRANSPARENT);
        view.addView(scrollView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return scrollView;


    }

    public interface callbackforReading {
        void onCallback(String value, EditText radioButton);

    }

    public void readData(final callbackforReading myCallback, DatabaseReference databaseReference, final EditText textView) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                myCallback.onCallback(value,textView);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    public interface callbackforStore{
        void onCallback(DatabaseReference databaseReference,String str);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        if (namesofQuiz.contains(object)) {
            return namesofQuiz.indexOf(object);
        } else {
            return POSITION_NONE;
        }
    }

    private void storeData(final callbackforStore myCallback,DatabaseReference databaseReference,String string){
      myCallback.onCallback(databaseReference,string);
    }

    private class MyClickListener implements TextWatcher {
        private int myParam;
        private int position;
        private EditText text;
        private Editable etext;
        private boolean isChange;

        public MyClickListener(int bestitz,int pos,EditText textview) {
            myParam = bestitz;
            position = pos;
            text = textview;
            etext = textview.getText();
        }

        public void afterTextChanged(Editable s) {
            // you can call or do what you want with your EditText here
            // yourEditText...
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            text.setSelection(s.toString().length());
            mReference = FirebaseDatabase.getInstance().getReference();
            DatabaseReference answer = mReference.child("answerCollection").child(getNamesofQuiz().get(position)).child("text" + String.valueOf(myParam));
            storeData(new callbackforStore() {
                @Override
                public void onCallback(DatabaseReference databaseReference, String str) {
                    databaseReference.setValue(str);
                }
            },answer,s.toString());

        }
    }
}