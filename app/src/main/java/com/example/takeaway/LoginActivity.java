

package com.example.takeaway;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {
    DatabaseReference mReference;
    private String username,password;
    private String usernameEntered, passwordEntered;
    EditText etUsername;
    EditText etPassword;
    Button btnLogin;
    ImageView back,icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        initFields();
        getdatafromRealtime(new Mycallback() {
            @Override
            public void oncallback(String username, String password) {
                setUsername(username);
                setPassword(password);
            }
        });
        addClicktobtnLogin();
    }

    private void initViews(){
        etUsername = findViewById(R.id.inputUsername);
        etPassword = findViewById(R.id.inputPassword);
        btnLogin = findViewById(R.id.buttonLogin);
        back = findViewById(R.id.backIcon);
        icon = findViewById(R.id.login);
    }

    private void initFields(){
        etUsername.setHint(R.string.fui_name_hint);
        etPassword.setHint(R.string.fui_password_hint);
        etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        etPassword.setSelection(etPassword.getText().length());
        btnLogin.setText("Login");
        icon.setImageResource(R.drawable.logo2);
        back.setImageResource(R.drawable.backicon);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getdatafromRealtime(final Mycallback myCallback){
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference admininfo = mReference.child("AdminInfo");
        admininfo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myCallback.oncallback(dataSnapshot.child("account").getValue(String.class),dataSnapshot.child("password").getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
    }

    private interface Mycallback{
        void oncallback(String username,String password);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    private void addClicktobtnLogin(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // update();
                usernameEntered = etUsername.getText().toString();
                passwordEntered = etPassword.getText().toString();

                if (usernameEntered.equals(getUsername()) && passwordEntered.equals(getPassword())) {
                    Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, "Incorrect username or password", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, WelcomeActivity.class);
        startActivity(intent);
    }
}
