
/*
 * *
 *   Copyright (C) 2019
 *   Author : Ruikang Xu
 *
 *
 */

package com.example.takeaway;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.takeaway.Adapter.AnswerAdapter;
import com.example.takeaway.Adapter.CodeAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class QuizchallengeActivity extends AppCompatActivity {
    private CodeAdapter mAdapter;
    private AnswerAdapter answerAdapter;
    private Button addButton;
    private DatabaseReference mReference;
    private static int numofQuizs;
    private ArrayList<String> nameofAnswers, nameofimages;
    ViewPager viewpager, viewPager3;
    ImageView logo, formBtn;
    String answersCollection;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        initFieldAndViews();
        toFormActivity();
        toBackpage();
    }

    private void toBackpage() {
        formBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        backDialog();
    }

    private void backDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(QuizchallengeActivity.this);
        builder.setMessage("Current answers will not be saved, Do you want to continue?");
        builder.setTitle("Warning");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent formIntent = new Intent(QuizchallengeActivity.this, VideoActivity.class);
                startActivity(formIntent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public ArrayList<String> getNameofAnswers() {
        return nameofAnswers;
    }

    public ArrayList<String> getNameofimages() {
        return nameofimages;
    }

    public void initFieldAndViews() {
        answersCollection = "";
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                numofQuizs = (int) dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        if (getIntent().getStringExtra("bestitz") != null) {
            setNumofQuizs(Integer.parseInt(getIntent().getStringExtra("bestitz")));
        }
        if (getIntent().getStringArrayListExtra("NAMEOFIMAGES") != null) {
            this.nameofimages = getIntent().getStringArrayListExtra("NAMEOFIMAGES");
        }
        if (getIntent().getStringArrayListExtra("NAMEOFANSWERS") != null) {
            this.nameofAnswers = getIntent().getStringArrayListExtra("NAMEOFANSWERS");
        }
        mAdapter = new CodeAdapter(numofQuizs, getNameofimages());
        answerAdapter = new AnswerAdapter(numofQuizs, getNameofAnswers());
        addButton = (Button) findViewById(R.id.add);
        addButton.setText(R.string.submit);
        logo = findViewById(R.id.logo);
        formBtn = findViewById(R.id.formBtn);
        formBtn.setImageResource(R.drawable.backicon);
        viewpager = findViewById(R.id.choosedImage);
        viewPager3 = findViewById(R.id.newAnswers);
        CircleIndicator indicator = findViewById(R.id.indicator2);
        viewPager3.setAdapter(answerAdapter);
        viewpager.setAdapter(mAdapter);
        indicator.setViewPager(viewpager);
        logo.setImageResource(R.drawable.logo);
        mAdapter.registerDataSetObserver(indicator.getDataSetObserver());
        answerAdapter.registerDataSetObserver(indicator.getDataSetObserver());

        //stick two viewPager
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int mScrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
                viewPager3.scrollTo(viewpager.getScrollX(), viewPager3.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {
                mScrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewPager3.setCurrentItem(viewpager.getCurrentItem(), false);
                }
            }
        });
        viewPager3.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int mScrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
                viewpager.scrollTo(viewPager3.getScrollX(), viewpager.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {
                mScrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewpager.setCurrentItem(viewPager3.getCurrentItem(), false);
                }
            }
        });
        //save each page in answer viewpager
        viewPager3.setOffscreenPageLimit(numofQuizs);
    }

    public void toFormActivity() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answerAdapter.isIsAllHit()) {
                    String value = "";
                    int counter = 1;
                    for (int i = 0; i < numofQuizs; i++) {
                        switch (answerAdapter.getChoice()[i]) {
                            case 1:
                                value = "A";
                                break;
                            case 2:
                                value = "B";
                                break;
                            case 3:
                                value = "C";
                                break;
                            case 4:
                                value = "D";
                                break;
                            case 5:
                                value = "E";
                                break;
                            case 6:
                                value = "F";
                                break;
                        }

                        answersCollection += value;
                        counter++;
                    }
                    dialog(answersCollection);
                } else {
                    Toast.makeText(QuizchallengeActivity.this, R.string.hint4, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void dialog(final String json) {
        final String jsonObject = json;
        AlertDialog.Builder builder = new AlertDialog.Builder(QuizchallengeActivity.this);
        builder.setMessage(R.string.quitQuithint);
        builder.setTitle(R.string.quitQuitTitle);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent;
                intent = new Intent(QuizchallengeActivity.this, FormActivity.class);
                intent.putExtra("json", jsonObject);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void setNumofQuizs(int num) {
        this.numofQuizs = num;
    }
}

