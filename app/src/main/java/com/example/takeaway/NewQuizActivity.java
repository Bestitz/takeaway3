
/*
 * *
 *   Copyright (C) 2019
 *   Author : Ruikang Xu
 *
 *
 */

package com.example.takeaway;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.takeaway.Moduel.AnswerModuel;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class NewQuizActivity extends AppCompatActivity {
    PhotoView photoView;
    ScrollView scrollView;
    Button addButton,save;
    DatabaseReference mReference;
    FirebaseStorage storage;
    StorageReference storageReference;
    List<String> nameofList;
    List<String> nameofImages;
    private final static int ADDANEWIMAGE = 8527;
    private final int PICK_IMAGE_REQUEST = 71;
    private final static int NUMOFANSWER = 6;
    private final static int EDITTEXTMARK = 183729;
    private int choice;
    private Uri filePath;
    private String imageUrl;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newquiz);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        refresh();
        initViews();
        initFields();
        setClicktoAdd();
        setSaveClick();

        if(getIntent().getStringExtra("FILEPATH") != null){
            String str = getIntent().getStringExtra("FILEPATH");
            filePath =  Uri.parse(str);
            Log.d("7894161568456", "onCreate: " + str);
            Picasso.get().load(filePath).into(photoView);
        } else {
            photoView.setImageResource(R.drawable.takeawaywhitelogo);
        }


    }

    private void refresh() {
        mReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference answerCollection = mReference.child("answerCollection");
        answerCollection.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                choice  = (int) dataSnapshot.getChildrenCount();
                nameofList = new ArrayList<String>();
                nameofImages = new ArrayList<String>();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String answerName = ds.getKey();
                    nameofList.add(answerName);
                    String imgeUrl = ds.child("imageUrl").getValue(String.class);
                    nameofImages.add(imgeUrl);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });
        answerCollection.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String string = dataSnapshot.getKey();
                nameofList.remove(string);
               // answerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initFields() {
        mReference = FirebaseDatabase.getInstance().getReference();
        //通过键名，获取数据库实例对象的子节点对象
        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        for (int i = 0; i < NUMOFANSWER; i++) {
            LinearLayout choice = new LinearLayout(getApplicationContext());
            TextView alphaLetter = new TextView(getApplicationContext());
            String value = "";
            switch (i + 1) {
                case 1:
                    value = "A: ";
                    break;
                case 2:
                    value = "B: ";
                    break;
                case 3:
                    value = "C: ";
                    break;
                case 4:
                    value = "D: ";
                    break;
                case 5:
                    value = "E: ";
                    break;
                case 6:
                    value = "F: ";
                    break;
            }
            alphaLetter.setText(value);
            alphaLetter.setGravity(Gravity.CENTER);
            alphaLetter.setTextColor(Color.WHITE);
            alphaLetter.setTextSize(48);
            alphaLetter.setId(i);
            DatabaseReference answer;
            EditText textView = new EditText(getApplicationContext());
            Log.d("jiku", "instantiateItem: " + textView.getText().toString() );
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(Color.WHITE);
            textView.setTextSize(48);
            textView.setId(EDITTEXTMARK+i);
            choice.addView(alphaLetter);
            choice.addView(textView);
            linearLayout.addView(choice);
        }
        scrollView.addView(linearLayout);
        scrollView.setBackgroundColor(Color.TRANSPARENT);
    }

    private void initViews(){
        photoView = findViewById(R.id.choosedImage);
        scrollView = findViewById(R.id.newAnswers);
        addButton = findViewById(R.id.add);
        save = findViewById(R.id.formBtn);
        save.setText("Upload");
        addButton.setText("Choose Image");
    }

    private void setSaveClick() {
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filePath != null){
                    uploadImage();
                } else {
                    Toast.makeText(NewQuizActivity.this, "Please choose an image First!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void uploadAnswers(String str) {
        if (str != null) {
            String[] strings = new String[NUMOFANSWER];
            for (int i = 0; i < NUMOFANSWER; i++) {
                EditText nweditText = (EditText) findViewById(EDITTEXTMARK + i);
                if (nweditText != null) {
                    strings[i] = nweditText.getText().toString();
                }
            }
            //添加URL
            AnswerModuel nwAnswer = new AnswerModuel(strings,str);
            DatabaseReference answerCollection = mReference.child("answerCollection");
            String id = UUID.randomUUID().toString();
            DatabaseReference myRef = answerCollection.child(id);
            myRef.setValue(nwAnswer);
            refresh();
        }
    }

    private void uploadImage() {
        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(R.string.upload);
            progressDialog.show();
            StorageReference ref = storageReference.child("QuizImages/" + UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (taskSnapshot.getMetadata() != null) {
                                if (taskSnapshot.getMetadata().getReference() != null) {
                                    Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                                    result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            String imageUrl = uri.toString();
                                            uploadAnswers(imageUrl);
                                            new CountDownTimer(2500, 1000) {
                                                public void onFinish() {
                                                    // When timer is finished
                                                    // Execute your code here
                                                    onBackPressed();
                                                }

                                                public void onTick(long millisUntilFinished) {
                                                    // millisUntilFinished    The amount of time until finished.
                                                }
                                            }.start();
                                        }
                                    });
                                }
                            }
                            progressDialog.dismiss();
                            Toast.makeText(NewQuizActivity.this, R.string.uploaded, Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(NewQuizActivity.this, R.string.failed +e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage(R.string.uploaded+(int)progress+"%");
                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                photoView.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void setClicktoAdd() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewQuizActivity.this, UpdateImageActivity.class);
                intent.putExtra("addImage", ADDANEWIMAGE + "");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent;
        intent = new Intent(NewQuizActivity.this, EditActivity.class);
        intent.putExtra("bestitz",getChoice() + "");
        intent.putExtra("NAMEOFANSWERS",(ArrayList<String>)getNameofList());
        intent.putExtra("NAMEOFIMAGES",(ArrayList<String>)getNameofImages());
        startActivity(intent);
    }

    public List<String> getNameofImages() {
        return nameofImages;
    }

    public List<String> getNameofList() {
        return nameofList;
    }

    private int getChoice() {
        return choice;
    }
}
